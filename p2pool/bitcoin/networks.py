import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

@defer.inlineCallbacks
def get_subsidy(bitcoind, target):
    res = yield bitcoind.rpc_getblock(target)

    defer.returnValue(res)

nets = dict(
    summercoinv2=math.Object(
        P2P_PREFIX='80503420'.decode('hex'),
        P2P_PORT=33330,
        ADDRESS_VERSION=125,
        RPC_PORT=33333,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'summercoinv2address' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda bitcoind, target: get_subsidy(bitcoind, target),
        BLOCK_PERIOD=30, # s
        SYMBOL='SUM2',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'Summercoinv2') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/Summercoinv2/') if platform.system() == 'Darwin' else os.path.expanduser('~/.summercoinv2'), 'summercoinv2.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
        CHARITY_ADDRESS='a41702ff87222f21d0c2ae9376a1c83c5bd848f0'.decode('hex'),
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
